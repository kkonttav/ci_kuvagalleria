<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Galleria extends CI_Controller {
    public function __construct() {
        parent::__construct();
    }
    
    public function index($kansio='') {
        $kansio=  urlencode($kansio);
        $data["kansiot"]=$this->kansio_model->hae_kaikki();

        if(strlen($kansio)==0 && count($data['kansiot'])>0) {
            $kansio= $data['kansiot'][0];
            $kansio= substr($kansio,0, strlen($kansio)-1);
        }
        $data['valittu']=$kansio;
        $this->session->set_userdata('kansio',$kansio);
        
        $data['kuvat']=  $this->kuva_model->hae_kansion_kuvat($kansio);
        
        $data["sivupalkki"]="kansio/kansiot_view";
        $data["sisalto"]="kuva/kuvat_view";
        $this->load->view("template.php",$data);
    }
    
    public function lisaa_kansio() {
        $data = array(
            'nimi' => ''
            );
        $data["kansiot"]= $this->kansio_model->hae_kaikki();
        $data["valittu"]= $this->session->userdata("kansio");
        $data["sivupalkki"]= "kansio/kansiot_view";
        $data["sisalto"]="kansio/kansio_view";
        
        $this->load->view("template.php", $data);        
    }
    
    public function tallenna_kansio() {
        $kansio= $this->input->post("kansio");
        $this->kansio_model->lisaa($kansio);
        
        redirect("galleria/index/$kansio");
    }
    
    public function poista_kansio() {
        $kansio=  $this->session->userdata("kansio");
        $this->kansio_model->poista($kansio);
        $this->session->unset_userdata("kansio");
        
        redirect("galleria/index");
    }
    
    public function lisaa_kuva() {
        $data["kansiot"]=  $this->kansio_model->hae_kaikki();
        $data["valittu"]= $this->session->userdata("kansio");
        $data["sivupalkki"]="kansio/kansiot_view";
        $data["sisalto"]="kuva/kuva_view";
        
        $this->load->view("template.php", $data);
    }
    
    public function lataa_kuva() {
        $kansio=  $this->session->userdata("kansio");
        $this->kuva_model->lisaa($kansio);
        redirect("galleria/index/$kansio");
    }
    
    public function nayta_kuva($kuva) {
        $data["kuva"]=$kuva;
        $data["kansiot"]=  $this->kansio_model->hae_kaikki();
        $data["valittu"]= $this->session->userdata("kansio");
        $data["sivupalkki"]="kansio/kansiot_view";
        $data["sisalto"]="kuva/esitys_view";
        
        $this->load->view("template.php", $data);
    }
    
    public function poista_kuva($kuva) {
        $kansio=  $this->session->userdata("kansio");
        $this->kuva_model->poista($kansio,$kuva);
        redirect("galleria/index/$kansio");
    }
    
}