<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kuva_Model extends CI_Model {
    public function hae_kansion_kuvat($kansio) {
        return directory_map($this->config->item('upload_path') . '/' . $kansio . '/');
    }

    public function poista($kansio, $kuva) {
        $polku=  $this->config->item("upload_path") . '/' . $kansio . "/$kuva";
        unlink($polku);
        
        $tiedosto=  explode(".",$kuva);
        
        $thumb_kuva=$tiedosto[0] . "_thumb." . $tiedosto[1];
        $polku_thumb=  $this->config->item("upload_path") . '/' . $kansio . "/$thumb_kuva";
        unlink($polku_thumb);
    }

    public function lisaa($kansio) {
        $polku=  $this->config->item('upload_path') . '/' . $kansio . '/';
        $tiedosto=  $this->tallenna_kuva($polku);
        $this->luo_thumbnail($polku . $tiedosto);
        
    }
    
    private function tallenna_kuva($polku) {
         $config['upload_path']=$polku;
         $config['allowed_types']='gif|jpg|png';
         $config['max_size']='5000';
         $config['max_width']='3000';
         $config['max_height']='2000';
         
         $this->load->library('upload',$config);
         
         if(!$this->upload->do_upload("userfile")) {
             throw new Exception($this->upload->display_errors());
         }
         
         $kuvan_tiedot=  $this->upload->data();
         return $kuvan_tiedot['file_name'];             
    }
    
    private function luo_thumbnail($tiedosto) {
        $config['create_thumb']=TRUE;
        $config['image_library']='gd2';
        $config['source_image']=$tiedosto;
        $config['maintain_ration']=TRUE;
        $config['width']=100;
        $config['height']=100;
        $this->load->library('image_lib',$config);
        $this->image_lib->resize();
    }
}